    var arr = new Array();

    $(function() {
        if ($("#grid").val() == "") {
            $("#grid").dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "iDisplayLength": 25
            });
        }
    });

    function validateSave() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#nameTax').val() == "")
        {
            $('#nameTax').css({"border-color" : "#F00"});
        } else {
            $('#nameTax').css({"border-color" : ""});
            i++;
        }

        if ($('#percentageTax').val() == "")
        {
            $('#percentageTax').css({"border-color" : "#F00"});
        } else {
            $('#percentageTax').css({"border-color" : ""});
            i++;
        }

        if (i==2) {
            if($('#idTax').val() > 0) {
                saveUpdate();
            } else {
                save();
            }
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function save(){
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'tax/',
                    name: $('#nameTax').val().trim(),
                    percentage: $('#percentageTax').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function saveUpdate() {
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'tax/update',
                    id_tax: $('#idTax').val().trim(),
                    name: $('#nameTax').val().trim(),
                    percentage: $('#percentageTax').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function cleanInput() {
        $('#nameTax').val('');
        $('#percentageTax').val('');
    }

    function load() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
            var rowId = $(this).attr("row-key");
            loadModal(rowId);
        });

        $("#alter").attr('disabled',true);
        $("#deletar").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',true);
            }
            if(arr.length == 1){
                $("#alter").attr('disabled',false);
                $("#deletar").attr('disabled',false);
            }
            if(arr.length > 1){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',false);
            }
        });
    };

    function btnLoadModal() {
        loadModal(arr[0]);
    }

    function loadModal(rowId) {
        $('#modalTax').modal('show');
        $('#idTax').val(rowId);

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'tax/get',
                    id_tax: [rowId],
                },
            success: function(data){
                var data = JSON.parse(data);

                $('#nameTax').val(data[0].name.trim());
                $('#percentageTax').val(data[0].percentage);
            },
            async:false
        })
    }

    function deleteItem() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'tax/delete',
                    id_tax: arrayList,
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }
