    var arr = new Array();

    $(function() {
        if ($("#grid").val() == "") {
            $("#grid").dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "iDisplayLength": 25
            });
        } else {
            $('#tab_parcelas').addClass('active');
            $('#tab-content_parcelas').addClass('active');
        }
    });

    function cleanInput() {
        $('#nameProduct').val('');
    }

    function cleanItemInput() {
        $('#id_product').val('');
        $('#amountProduct').val('');
    }

    function load() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
            var rowId = $(this).attr("row-key");
            window.location.href = '/compra/'+rowId;
        });

        $("#alter").attr('disabled',true);
        $("#deletar").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',true);
            }
            if(arr.length == 1){
                $("#alter").attr('disabled',false);
                $("#deletar").attr('disabled',false);
            }
            if(arr.length > 1){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',false);
            }
        });
    };

    function loadSell() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
        });

        $("#deleteItems").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#deleteItems").attr('disabled',true);
            }
            if(arr.length > 0){
                $("#deleteItems").attr('disabled',false);
            }
        });
    };

    function alter(){
        window.location.href = '/compra/'+arr[0];
    }

    function alterItem() {
        var rowId = $(this).attr("row-key");
        $('#modalSellItems').modal('show');
    }


    function deleteItem() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'sell/delete',
                    id_sell: arrayList,
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function deleteItem2() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'item/delete',
                    id_item: arrayList,
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function loadSellTotal() {
        $("#sellTotal").load('../../view/sell/blocks/total.php?idSell='+$('#idSellSelected').val());
        loadList();
    }

    function loadList() {
        $("#parcelas").load('../../view/sell/blocks/list.php?idSell='+$('#idSellSelected').val());
    }

    function validateClientAdd() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#id_client').val() == " ")
        {
            $('#id_client').css({"border-color" : "#F00"});
        } else {
            $('#id_client').css({"border-color" : ""});
            i++;
        }

        if (i==1) {
            saveClientAdd();
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function validateProductAdd() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#id_product').val() == "" ||
            $('#id_product').val() == " " ||
            $('#id_product').val() == null)
        {
            $('#id_product').css({"border-color" : "#F00"});
        } else {
            $('#id_product').css({"border-color" : ""});
            i++;
        }

        if ($('#amountProduct').val() == "")
        {
            $('#amountProduct').css({"border-color" : "#F00"});
        } else {
            $('#amountProduct').css({"border-color" : ""});
            i++;
        }

        if (i==2) {
            saveProductAdd();
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function saveProductAdd() {
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'item/',
                    id_sell: $('#idSellSelected').val().trim(),
                    id_product: $('#id_product').val().trim(),
                    amount: $('#amountProduct').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function saveClientAdd() {
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'sell/',
                    id_client: $('#id_client').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }
