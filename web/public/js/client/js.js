    var arr = new Array();

    $(function() {
        if ($("#grid").val() == "") {
            $("#grid").dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "iDisplayLength": 25
            });
        }
    });

    function validateSave() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#nameClient').val() == "")
        {
            $('#nameClient').css({"border-color" : "#F00"});
        } else {
            $('#nameClient').css({"border-color" : ""});
            i++;
        }

        if (i==1) {
            save();
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function save(){
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'client/',
                    name: $('#nameClient').val()
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function cleanInput() {
        $('#nameClient').val('');
    }

    function load() {

        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
        });

        $("#alter").attr('disabled',true);
        $("#deletar").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',true);
            }
            if(arr.length == 1){
                $("#alter").attr('disabled',false);
                $("#deletar").attr('disabled',false);
            }
            if(arr.length > 1){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',false);
            }
        });
    };
