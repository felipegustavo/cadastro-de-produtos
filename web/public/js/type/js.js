    var arr = new Array();

    $(function() {
        if ($("#grid").val() == "") {
            $("#grid").dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "iDisplayLength": 25
            });
        }
    });

    function validateSave() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#nameType').val() == "")
        {
            $('#nameType').css({"border-color" : "#F00"});
        } else {
            $('#nameType').css({"border-color" : ""});
            i++;
        }

        if (i==1) {
            if($('#idType').val() > 0) {
                saveUpdate();
            } else {
                save();
            }
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function save(){
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'type/',
                    name: $('#nameType').val().trim()
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function saveUpdate() {
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'type/update',
                    id_type: $('#idType').val().trim(),
                    name: $('#nameType').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function cleanInput() {
        $('#nameType').val('');
    }

    function load() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
            var rowId = $(this).attr("row-key");
            loadModal(rowId);
        });

        $("#alter").attr('disabled',true);
        $("#deletar").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',true);
            }
            if(arr.length == 1){
                $("#alter").attr('disabled',false);
                $("#deletar").attr('disabled',false);
            }
            if(arr.length > 1){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',false);
            }
        });
    };

    function btnLoadModal() {
        loadModal(arr[0]);
    }

    function loadModal(rowId) {
        $('#modalType').modal('show');
        $('#idType').val(rowId);

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'type/get',
                    id_type: [rowId],
                },
            success: function(data){
                var data = JSON.parse(data);

                $('#nameType').val(data[0].name.trim());
            },
            async:false
        })
    }

    function deleteItem() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'type/delete',
                    id_type: arrayList,
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }
