    var arr = new Array();

    $(function() {
        if ($("#grid").val() == "") {
            $("#grid").dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "iDisplayLength": 25
            });
        } else {
            $('#tab_parcelas').addClass('active');
            $('#tab-content_parcelas').addClass('active');
        }
    });

    function validateSave() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#nameProduct').val() == "")
        {
            $('#nameProduct').css({"border-color" : "#F00"});
        } else {
            $('#nameProduct').css({"border-color" : ""});
            i++;
        }

        if ($('#id_type').val() == "" || $('#id_type').val() == " ")
        {
            $('#id_type').css({"border-color" : "#F00"});
        } else {
            $('#id_type').css({"border-color" : ""});
            i++;
        }

        if ($('#valueProduct').val() == "")
        {
            $('#valueProduct').css({"border-color" : "#F00"});
        } else {
            $('#valueProduct').css({"border-color" : ""});
            i++;
        }

        if (i==3) {
            save();
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function save(){
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'product/',
                    name: $('#nameProduct').val().trim(),
                    id_type: $('#id_type').val().trim(),
                    value: $('#valueProduct').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function cleanInput() {
        $('#nameProduct').val('');
    }

    function load() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
            var rowId = $(this).attr("row-key");
            window.location.href = '/produto/'+rowId;
        });

        $("#alter").attr('disabled',true);
        $("#deletar").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',true);
            }
            if(arr.length == 1){
                $("#alter").attr('disabled',false);
                $("#deletar").attr('disabled',false);
            }
            if(arr.length > 1){
                $("#alter").attr('disabled',true);
                $("#deletar").attr('disabled',false);
            }
        });
    };

    function loadProduct() {
        //  Duplo click
        $('.table-hover tbody tr').dblclick(function() {
        });

        $("#deleteTax").attr('disabled',true);

        //  Click simples
        $('.table-hover tbody tr').click(function() {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
               arr.push($(this).attr('row-key'));
            } else {
                var itemtoRemove = $(this).attr('row-key');
                arr.splice($.inArray(itemtoRemove, arr),1);
            }

            if(arr.length == 0){
                $("#deleteTax").attr('disabled',true);
            }
            if(arr.length > 0){
                $("#deleteTax").attr('disabled',false);
            }
        });
    };

    function alter(){
        window.location.href = '/produto/'+arr[0];
    }

    function deleteItem() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'product/delete',
                    id_product: arrayList,
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function deleteItem2() {
        var list = arr.toString();
        var arrayList = list.split(',').map(function(n) {
            return Number(n);
        });

        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'product/producthaxtaxdelete',
                    id_tax: arrayList,
                    id_product: $('#idProductSelected').val(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }

    function loadList() {
        $("#parcelas").load('../../view/product/blocks/list.php?idProduct='+$('#idProductSelected').val());
    }

    function loadModalTax() {
        $('#idProduct').val($('#idProductSelected').val())
    }

    function validateTaxAdd() {
        $("#btnSave").find("*").prop("disabled", true);

        var i=0;

        if ($('#idProduct').val() == " ")
        {
        } else {
            i++;
        }

        if ($('#id_tax').val() == " ")
        {
            $('#id_tax').css({"border-color" : "#F00"});
        } else {
            $('#id_tax').css({"border-color" : ""});
            i++;
        }

        if (i==2) {
            saveTaxAdd();
        } else {
            $("#btnSave").find("*").prop("disabled", false);
        }
    }

    function saveTaxAdd() {
        $.ajax({
            type: 'post',
            url: '../../../api/api.php',
            data: {
                    route: 'product/producthaxtax',
                    id_product: $('#idProduct').val().trim(),
                    id_tax: $('#id_tax').val().trim(),
                },
            success: function(){
                setTimeout(function() {
                    window.location.reload();
                }, 500);
            },
            async:false
        })
    }
