<?php

require_once($_SERVER['DOCUMENT_ROOT'].'api/controllers/ApiControllers.php');

class Combo extends ApiControllers
{
    private function componentConstruct($obj)
    {
        $arrAtivo = [];
        $arrInativo = [];

        foreach ($obj as $keyObj => $valueObj) {
            if(empty($obj[$keyObj]['excluido']) || $obj[$keyObj]['excluido'] == 0){
                $arrAtivo[] = ['id' => $obj[$keyObj]['id'], 'item' => $obj[$keyObj]['item'], 'request' => $obj[$keyObj]['request_']];
            }else{
                $arrInativo[] = ['id' => $obj[$keyObj]['id'], 'item' => $obj[$keyObj]['item'], 'request' => $obj[$keyObj]['request_']];
            }
        }

        $html = '<select name="name_'.$obj[0]['name_'].'" id="id_'.$obj[0]['id_'].'" class="selectpicker form-custom col-lg-12" '.$obj[0]['tipo_'].' '.$obj[0]['disabled_'].'>';
        $html .='<option> </option>';

        if(count($arrAtivo) > 0 && count($arrInativo) > 0 && isset($obj[0]['tipoCombo_'])){
            $html .='<optgroup label="Ativo">';
        }
        foreach ($arrAtivo as $keyArrAtivo => $valueArrAtivo) {
            if ($arrAtivo[$keyArrAtivo]['request'] == $arrAtivo[$keyArrAtivo]['id']) {
                $selected = 'selected';
            } else {
                $selected = '';
            }

            $html .='<option value="'.$arrAtivo[$keyArrAtivo]['id'].'" '.$selected.'>'.$arrAtivo[$keyArrAtivo]['item'].'</option>';
        }
        if(count($arrAtivo) > 0 && count($arrInativo) > 0){
            $html .='</optgroup>';
        }

        if(count($arrAtivo) == 0 && count($arrInativo) == 0) {
            $html .='<option> </option>';
        }

        if(isset($obj[0]['tipoCombo_']) && $obj[0]['tipoCombo_']){
            if(count($arrInativo) > 0){
                $html .='<optgroup label="Excluído" disabled>';
            }
            foreach ($arrInativo as $keyArrInativo => $valueArrInativo) {
                if ($arrInativo[$keyArrInativo]['request'] == $arrInativo[$keyArrInativo]['id']) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }

                $html .='<option value="'.$arrInativo[$keyArrInativo]['id'].'" '.$selected.'>'.$arrInativo[$keyArrInativo]['item'].'</option>';
            }
            if(count($arrInativo) > 0){
                $html .='</optgroup>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function product($obj)
    {
        $objs = $this->dataProduct();

        $arr = array();
        foreach ($objs as $key => $value) {
            $arr[] = ['id' => $objs[$key]['id_product'],
                      'item' => $objs[$key]['name'],
                      'excluido' => $objs[$key]['situation'],
                      'id_' => $obj['id'],
                      'name_' => $obj['name'],
                      'request_' => (isset($obj['request']) ? $obj['request']:0),
                      'tipo_' => (isset($obj['tipo'])? $obj['tipo']:''),
                      'tipoCombo_' => (isset($obj['tipoCombo']) ? $obj['tipoCombo']:null),
                      'disabled_' => (isset($obj['disabled']) ? $obj['disabled']:'')];
        }

        return $this->componentConstruct($arr);
    }

    public function dataProduct()
    {
        $data = $this->get([
            'route' => 'product/get',
            'situation' => 0,
        ]);

        return json_decode($data, true);
    }

    public function tax($obj)
    {
        $objs = $this->dataTax();

        $arr = array();
        foreach ($objs as $key => $value) {
            $arr[] = ['id' => $objs[$key]['id_tax'],
                      'item' => $objs[$key]['name'].' - '.$objs[$key]['percentage'].' %',
                      'excluido' => $objs[$key]['situation'],
                      'id_' => $obj['id'],
                      'name_' => $obj['name'],
                      'request_' => (isset($obj['request']) ? $obj['request']:0),
                      'tipo_' => (isset($obj['tipo'])? $obj['tipo']:''),
                      'tipoCombo_' => (isset($obj['tipoCombo']) ? $obj['tipoCombo']:null),
                      'disabled_' => (isset($obj['disabled']) ? $obj['disabled']:'')];
        }

        return $this->componentConstruct($arr);
    }

    public function dataTax()
    {
        $data = $this->get([
            'route' => 'tax/get',
            'situation' => 0,
        ]);

        return json_decode($data, true);
    }

    public function client($obj)
    {
        $objs = $this->dataClient();

        $arr = array();
        foreach ($objs as $key => $value) {
            $arr[] = ['id' => $objs[$key]['id_client'],
                      'item' => $objs[$key]['name'],
                      'excluido' => 0,
                      'id_' => $obj['id'],
                      'name_' => $obj['name'],
                      'request_' => (isset($obj['request']) ? $obj['request']:0),
                      'tipo_' => (isset($obj['tipo'])? $obj['tipo']:''),
                      'tipoCombo_' => (isset($obj['tipoCombo']) ? $obj['tipoCombo']:null),
                      'disabled_' => (isset($obj['disabled']) ? $obj['disabled']:'')];
        }

        return $this->componentConstruct($arr);
    }

    private function dataClient()
    {
        $data = $this->get([
            'route' => 'client/get',
            'situation' => 0,
        ]);

        return json_decode($data, true);
    }

    public function type($obj)
    {
        $objs = $this->dataType();

        $arr = array();
        foreach ($objs as $key => $value) {
            $arr[] = ['id' => $objs[$key]['id_type'],
                      'item' => $objs[$key]['name'],
                      'excluido' => $objs[$key]['situation'],
                      'id_' => $obj['id'],
                      'name_' => $obj['name'],
                      'request_' => (isset($obj['request']) ? $obj['request']:0),
                      'tipo_' => (isset($obj['tipo'])? $obj['tipo']:''),
                      'tipoCombo_' => (isset($obj['tipoCombo']) ? $obj['tipoCombo']:null),
                      'disabled_' => (isset($obj['disabled']) ? $obj['disabled']:'')];
        }

        return $this->componentConstruct($arr);
    }

    private function dataType()
    {
        $data = $this->get([
            'route' => 'type/get',
            'situation' => 0,
        ]);

        return json_decode($data, true);
    }
}

?>
