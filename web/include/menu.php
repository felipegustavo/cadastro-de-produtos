    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Menu">
    <meta name="author" content="Felipe Gustavo Andrzejewski">

    <title>Se</title>

    <head>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" type="image/x-icon" href="../../public/img/img.ico">

        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/compra/">SE</a>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                            <li><a href="/compra/">Compras<span class="badge badge-custom" id="badge-prospect" name="badge-prospect"></span></a></li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">Cadastros <b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-custom">
                                    <li><a href="/cliente/">Clientes</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/produto/">Produtos</a></li>
                                    <li><a href="/tipo/">Tipos</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/taxa/">Taxas</a></li>
                                </ul>
                            </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <span class="visible-md visible-lg">
                                    <div style="margin-right:15px">
                                        <img class="img-circle media-object" style="width:20px;" src="../../public/img/avatar.png" alt="Profile Picture">
                                    </div>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </head>
