<?php

class Table
{

    function __construct($id, $arrTitle, $arrItems, $arrAlign)
    {
        $html = '   <div class="box">
                    <div class="box-body">
                        <table id="'.$id.'" class="table table-hover">

                            <thead class="table-striped">
                                <tr>    ';

                                foreach ($arrTitle as $keyArrTitle => $valueArrTitle) {
        $html .= '                  <th>';
        $html .= '                      <p style="font-weight: bold" align="'.$arrAlign[$keyArrTitle].'">'.$arrTitle[$keyArrTitle].'</p>';
        $html .= '                  </th>';
                                }

        $html .= '              </tr>
                            </thead>

                            <tbody>';

                                $colunas = (isset($arrItems[0]))?count($arrItems[0]):0;
                                foreach ($arrItems as $keyArrItems => $valueArrItems) {
        $html .= '                  <tr row-key="'.$arrItems[$keyArrItems][0].'">';

                                    for ($i=1; $i<$colunas; $i++) {
        $html .= '                      <td style="vertical-align: middle;" align="'.$arrAlign[$i-1].'">'.$arrItems[$keyArrItems][$i].'</td>';
                                    }

        $html .= '                  </tr>';
                                }

        $html .= '          </tbody>

                            <tfoot>
                                <tr>';
                                foreach ($arrTitle as $keyArrTitle => $valueArrTitle) {
        $html .= '                  <th>';
        $html .= '                      <p style="font-weight: bold" align="'.$arrAlign[$keyArrTitle].'">'.$arrTitle[$keyArrTitle].'</p>';
        $html .= '                  </th>';
                                }
        $html .= '              </tr>
                            </tfoot>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->    ';

        echo $html;
    }

}

?>
