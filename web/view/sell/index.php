<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/sell/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'sell/listtotal',
            ]);
            $data = json_decode($data, true);

            $id = 'grid';
            $arrTitle = ['Código compra', 'Nome', 'Data compra', 'R$ total', 'R$ taxa'];
            $arrAlign = ['left', 'left', 'left', 'left', 'right'];
            $arrItems = [];

            foreach ($data as $key => $value) {
                $arrItems[$key][] = $value['id_sell'];
                $arrItems[$key][] = $value['id_sell'];
                $arrItems[$key][] = $value['name_client'];
                $arrItems[$key][] = $value['sell_date'];
                $arrItems[$key][] = $value['value_total'];
                $arrItems[$key][] = $value['value_tax'];
            }

            $arrItems = array_values($arrItems);
        ?>

        <script src="../../public/js/sell/js.js"></script>

        <body>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" id="deletar" name="deletar" class="btn btn-primary block" data-toggle="modal" data-target="#modalDelete">Deletar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="alter" name="alter" class="btn btn-primary block" onclick="alter()">Adicionar Itens</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalSell" onclick="cleanInput()" title="Cadastro da produto">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="fbox float-e-margins">
                    <?php
                        new Table($id, $arrTitle, $arrItems, $arrAlign);
                    ?>
                </div>
            </div>

            <script type="text/javascript">
                load();
            </script>
        </body>
    </html>
