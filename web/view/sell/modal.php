    <div id="modalSell" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalSellLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalSellLabel">Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idSell" id="idSell">
                        <div>
                            <div class="col-sm-12 col-lg-12">
                                <p for="client">Nome <small>(*)</small></p>
                                <?php
                                    $combo = new Combo();

                                    $obj = array('id' => 'client', 'name' => 'client', 'request' => isset($arr['tipo_valor'])?$arr['tipo_valor']:'');
                                    echo $combo->client($obj);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateClientAdd()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalSellItems" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalSellItemsLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalSellItemsLabel">Produto</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idSellItems" id="idSellItems">
                        <div>
                            <div class="col-sm-8 col-lg-8">
                                <p for="product">Nome <small>(*)</small></p>
                                <?php
                                    $combo = new Combo();

                                    $obj = array('id' => 'product', 'name' => 'product', 'request' => isset($arr['tipo_valor'])?$arr['tipo_valor']:'');
                                    echo $combo->product($obj);
                                ?>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <p for="amountProduct">Quantidade <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="amountProduct" name="amountProduct">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateProductAdd()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
