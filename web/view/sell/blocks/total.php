<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'api/controllers/ApiControllers.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'sell/listtotal',
                'id_sell' => $_GET['idSell'],
            ]);
            $data = json_decode($data, true);
            $data = reset($data);
        ?>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="text-right texto-cinza-claro">
                <small>Valor total</small>
                <small><?php echo $data['value_total']; ?></small>
            </div>
            <div class="text-right texto-cinza-claro padding-bottom-10">
                <small>Valor taxa</small>
                <small><?php echo $data['value_tax']; ?></small>
            </div>
        </div>
    </html>
