<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'include/Table.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'api/controllers/ApiControllers.php');

    $api = new ApiControllers();
    $data = $api->get([
        'route' => 'sell/list',
        'id_sell' => $_GET['idSell'],
    ]);
    $data = json_decode($data, true);

    $id = 'gridList';

    $arrTitulo = array('Codigo Item', 'Produto', 'Tipo', 'Quantidade', 'R$ item', 'R$ total', 'R$ taxa');
    $arrAlign = array('left', 'left', 'left', 'left', 'left', 'left', 'right');
    $arrItems = [];

    foreach ($data as $key => $value) {
        $arrItems[$key][] = $value['id_item'];
        $arrItems[$key][] = $value['id_item'];
        $arrItems[$key][] = $value['name_product'];
        $arrItems[$key][] = $value['type_product'];
        $arrItems[$key][] = $value['amount'];
        $arrItems[$key][] = $value['value_item'];
        $arrItems[$key][] = $value['value_total'];
        $arrItems[$key][] = $value['value_tax'];
    }

    $arrItems = array_values($arrItems);
?>

    <div>
        <?php
            new Table($id, $arrTitulo, $arrItems, $arrAlign);
        ?>
    </div>

    <script src="../../public/js/sell/js.js"></script>
    <script type="text/javascript">
        loadSell();
    </script>
