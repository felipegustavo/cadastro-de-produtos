<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/sell/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'sell/get',
                'id_sell' => $_GET['sell'],
            ]);
            $data = json_decode($data, true);
            $data = reset($data);
        ?>

        <script src="../../public/js/sell/js.js"></script>

        <body>
            <input type="hidden" name="idSellSelected" id="idSellSelected" value="<?php echo $_GET['sell'];?>">
            <div class="col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" id="deleteItems" name="deleteItems" class="btn btn-primary block" data-toggle="modal" data-target="#modalDelete2">Deletar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalSellItems" title="Cadastro de novo item" onclick="cleanItemInput()">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div id="boxTipoFatura" name="boxTipoFatura" class="fbox float-e-margins col-xs-12 col-sm-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h3><?php echo $data['name_client'];?></h3>
                </div>
            </div>

            <div class="fbox-sub col-xs-12 col-sm-12 col-md-12 bs-callout text-center bg-grey">
                <div class="col-xs-12 col-sm-7 col-md-7">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div id="sellTotal"></div>
                </div>
                <div class="col-xs-12 col-sm-1 col-md-1">
                </div>
            </div>

            <div name='divGeral' id='divGeral'>
                <div class="fbox-sub float-e-margins col-xs-12 col-sm-12 col-md-12 bs-callout">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li id="tab_parcelas">
                            <a href="#tab-content_parcelas" data-toggle="tab">Itens</a>
                        </li>
                    </ul>
                    <div id="my-tab-content" class="tab-content">
                        <div class="tab-pane" id="tab-content_parcelas">
                            <div class="fbox-sub float-e-margins col-xs-12 col-sm-12 col-md-12">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div id="parcelas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>

            <script type="text/javascript">
                loadSellTotal();
            </script>
        </body>
    </html>
