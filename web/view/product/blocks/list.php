<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'include/Table.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'api/controllers/ApiControllers.php');

    $api = new ApiControllers();
    $data = $api->get([
        'route' => 'tax/get',
        'id_product' => $_GET['idProduct'],
    ]);
    $data = json_decode($data, true);

    $id = 'gridList';

    $arrTitulo = array('Código', 'Nome', 'Percentual');
    $arrAlign = array('left', 'left', 'right');
    $arrItems = [];

    foreach ($data as $key => $value) {
        $arrItems[$key][] = $value['id_tax'];
        $arrItems[$key][] = $value['id_tax'];
        $arrItems[$key][] = $value['name'];
        $arrItems[$key][] = $value['percentage'].' %';
    }

    $arrItems = array_values($arrItems);
?>

    <div>
        <?php
            new Table($id, $arrTitulo, $arrItems, $arrAlign);
        ?>
    </div>

    <script src="../../public/js/product/js.js"></script>
    <script type="text/javascript">
        loadProduct();
    </script>
