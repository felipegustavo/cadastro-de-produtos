<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/product/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'product/get',
            ]);
            $data = json_decode($data, true);

            $situation = ['Ativo', 'Deletado'];

            $id = 'grid';
            $arrTitle = ['Código', 'Nome', 'Tipo', 'Valor', 'Situação'];
            $arrAlign = ['left', 'left', 'left', 'left', 'right'];
            $arrItems = [];

            foreach ($data as $key => $value) {
                $arrItems[$key][] = $value['id_product'];
                $arrItems[$key][] = $value['id_product'];
                $arrItems[$key][] = $value['name'];
                $arrItems[$key][] = $value['name_type_product'];
                $arrItems[$key][] = $value['value'];
                $arrItems[$key][] = $situation[$value['situation']];
            }

            $arrItems = array_values($arrItems);
        ?>

        <script src="../../public/js/product/js.js"></script>

        <body>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" id="deletar" name="deletar" class="btn btn-primary block" data-toggle="modal" data-target="#modalDelete">Deletar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="alter" name="alter" class="btn btn-primary block" onclick="alter()">Cadastrar taxas</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalProduct" onclick="cleanInput()" title="Cadastro de produto">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="fbox float-e-margins">
                    <?php
                        new Table($id, $arrTitle, $arrItems, $arrAlign);
                    ?>
                </div>
            </div>

            <script type="text/javascript">
                load();
            </script>
        </body>
    </html>
