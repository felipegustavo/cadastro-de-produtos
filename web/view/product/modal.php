    <div id="modalProduct" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalProductLabel">Produto</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idProduct" id="idProduct">
                        <div>
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <p for="nameProduct">Nome <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="nameProduct" name="nameProduct">
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <p for="type">Tipo <small>(*)</small></p>
                                <?php
                                    $combo = new Combo();

                                    $obj = array('id' => 'type', 'name' => 'type', 'request' => isset($arr['type'])?$arr['type']:'');
                                    echo $combo->type($obj);
                                ?>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <p for="valueProduct">Valor <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="valueProduct" name="valueProduct">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateSave()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalProductTax" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalProductTaxLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalProductTaxLabel">Taxas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idProduct" id="idProduct">
                        <div>
                            <div class="col-sm-12 col-lg-12">
                                <p for="tax">Nome <small>(*)</small></p>
                                <?php
                                    $combo = new Combo();

                                    $obj = array('id' => 'tax', 'name' => 'tax', 'request' => '');
                                    echo $combo->tax($obj);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateTaxAdd()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
