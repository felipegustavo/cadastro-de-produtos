<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/product/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'product/get',
                'id_product' => $_GET['product'],
            ]);
            $data = json_decode($data, true);
            $data = reset($data);
        ?>

        <script src="../../public/js/product/js.js"></script>

        <body>
            <input type="hidden" name="idProductSelected" id="idProductSelected" value="<?php echo $_GET['product'];?>">
            <div class="col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" id="deleteTax" name="deleteTax" class="btn btn-primary block" data-toggle="modal" data-target="#modalDelete2">Deletar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalProductTax" title="Cadastro de taxa" onclick="loadModalTax()">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div id="boxTipoFatura" name="boxTipoFatura" class="fbox float-e-margins col-xs-12 col-sm-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h3><?php echo $data['name_type_product'];?> <small><?php echo $data['name'];?></small></h3>
                </div>
            </div>

            <div name='divGeral' id='divGeral'>
                <div class="fbox-sub float-e-margins col-xs-12 col-sm-12 col-md-12 bs-callout">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li id="tab_parcelas">
                            <a href="#tab-content_taxas" data-toggle="tab">Taxas</a>
                        </li>
                    </ul>
                    <div id="my-tab-content" class="tab-content">
                        <div class="tab-pane" id="tab-content_parcelas">
                            <div class="fbox-sub float-e-margins col-xs-12 col-sm-12 col-md-12">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div id="parcelas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>

            <script type="text/javascript">
                loadList();
            </script>
        </body>
    </html>
