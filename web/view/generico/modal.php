    <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    Tem certeza que deseja deletar?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                    <div class="btn-group" id="btnSalvar" name="btnSalvar">
                        <button type="button" class="btn btn-primary" onclick="deleteItem()">Deletar</button>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div id="modalDelete2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel2">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">

                <input type="hidden" name="temp" id="temp">

                <div class="modal-body">
                    Tem certeza que deseja deletar?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                    <div class="btn-group" id="btnSalvar" name="btnSalvar">
                        <button type="button" class="btn btn-primary" onclick="deleteItem2()">Deletar</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
