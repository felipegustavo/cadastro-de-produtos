    <div id="modalType" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTypeLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalTypeLabel">Tipo</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idType" id="idType">
                        <div>
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <p for="nameType">Nome <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="nameType" name="nameType">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateSave()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
