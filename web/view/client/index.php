<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/client/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'/view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'client/get',
            ]);
            $data = json_decode($data, true);

            $situation = ['Ativo', 'Deletado'];

            $id = 'grid';
            $arrTitle = ['Código', 'Nome'];
            $arrAlign = ['left', 'left'];
            $arrItems = [];

            foreach ($data as $key => $value) {
                $arrItems[$key][] = $value['id_client'];
                $arrItems[$key][] = $value['id_client'];
                $arrItems[$key][] = $value['name'];
            }
        ?>

        <script src="../../public/js/client/js.js"></script>

        <body>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalClient" onclick="cleanInput()" title="Cadastro de cliente">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="fbox float-e-margins">
                    <?php
                        new Table($id, $arrTitle, $arrItems, $arrAlign);
                    ?>
                </div>
            </div>

            <script type="text/javascript">
                load();
            </script>
        </body>
    </html>
