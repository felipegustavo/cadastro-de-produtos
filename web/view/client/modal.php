    <div id="modalClient" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalClientLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalClientLabel">Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idClient" id="idClient">
                        <div>
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <p for="nameClient">Nome <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="nameClient" name="nameClient">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateSave()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
