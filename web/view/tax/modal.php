    <div id="modalTax" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTaxLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalTaxLabel">Taxa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="idTax" id="idTax">
                        <div>
                            <div class="col-sm-8 col-lg-8">
                                <div class="form-group">
                                    <p for="nameTax">Nome <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="nameTax" name="nameTax">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <p for="percentageTax">Percentual <small>(*)</small></p>
                                    <input type="text" class="form-custom" id="percentageTax" name="percentageTax">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <div class="btn-group" id="btnSave" name="btnSave">
                        <button type="button" class="btn btn-primary" onclick="validateSave()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
