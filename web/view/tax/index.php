<!DOCTYPE html>
    <html lang="en">

        <?php
            require_once($_SERVER['DOCUMENT_ROOT'].'include/generic.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'view/tax/modal.php');
            require_once($_SERVER['DOCUMENT_ROOT'].'view/generico/modal.php');

            $api = new ApiControllers();
            $data = $api->get([
                'route' => 'tax/get',
            ]);
            $data = json_decode($data, true);

            $situation = ['Ativo', 'Deletado'];

            $id = 'grid';
            $arrTitle = ['Código', 'Nome', 'Percentual', 'Situação'];
            $arrAlign = ['left', 'left', 'center', 'right'];
            $arrItems = [];

            foreach ($data as $key => $value) {
                $arrItems[$key][] = $value['id_tax'];
                $arrItems[$key][] = $value['id_tax'];
                $arrItems[$key][] = $value['name'];
                $arrItems[$key][] = $value['percentage'].' %';
                $arrItems[$key][] = $situation[$value['situation']];
            }
        ?>

        <script src="../../public/js/tax/js.js"></script>

        <body>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="margin">
                    <div class="btn-group">
                        <button type="button" id="deletar" name="deletar" class="btn btn-primary block" data-toggle="modal" data-target="#modalDelete">Deletar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="alter" name="alter" class="btn btn-primary block" onclick="btnLoadModal()">Alterar</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary block" data-toggle="modal" data-target="#modalTax" onclick="cleanInput()" title="Cadastro da taxa">+</button>
                    </div>
                </div>
                <br>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="fbox float-e-margins">
                    <?php
                        new Table($id, $arrTitle, $arrItems, $arrAlign);
                    ?>
                </div>
            </div>

            <script type="text/javascript">
                load();
            </script>
        </body>
    </html>
