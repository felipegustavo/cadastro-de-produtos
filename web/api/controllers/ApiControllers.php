<?php
require_once($_SERVER['DOCUMENT_ROOT'].'api/mappers/ApiMappers.php');

Class ApiControllers
{
    public function get($params)
    {
        $api = new ApiMappers();
        return $api->get($params);
    }

    public function post($params)
    {
        $api = new ApiMappers();
        return $api->post($params);
    }
}
