<?php

Class ApiMappers
{
    private $token;

    public function __construct()
    {
        $this->token = 'qUFVBL1A478LJT0OD6HF209R1D0MXXA07V0RZYM4EJIZRBV3Q155WGP40FA3MQWXIQBMVMBECD43ON8LG3EQHFFF02X02ZBJQVK3';
    }

    public function get($params)
    {
        $curl = curl_init();

        $paramsGet = '';
        $clone = $params;
        unset($clone['route']);
        foreach ($clone as $key => $value) {
            $paramsGet .= $key.'='.$value.'&';
        }

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://localhost:8005/api/collections/'.$params['route'].'?'.$paramsGet.'token='.$this->token
        ));

        $json = curl_exec($curl);
        curl_close($curl);

        return $json;
    }

    public function post($params)
    {
        $cURL = curl_init();

        curl_setopt($cURL, CURLOPT_URL, 'http://localhost:8005/api/collections/'.$params['route']);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);

        unset($params['route']);
        $params['token'] = $this->token;

        curl_setopt($cURL, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));

        $json = curl_exec($cURL);
        curl_close($cURL);

        return $json;
    }
}
