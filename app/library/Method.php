<?php

class Method
{
    public function define($method)
    {
        if (empty($method)) {
            return [
                'error' => 'Method not exist.'
            ];
        }
        if (!in_array(strtoupper($method), ['GET', 'POST'])) {
            return [
                'error' => 'Method '.$method.' is invalid or not supported.'
            ];
        }

        $method = strtolower($method);
        return $this->$method();
    }

    private function get()
    {
        return $_GET;
    }

    private function post()
    {
        return $_POST;
    }
}
