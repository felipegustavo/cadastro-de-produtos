<?php

class Autoload
{
    public function __construct()
    {
        spl_autoload_register(
            function($class) {
                require_once($_SERVER['DOCUMENT_ROOT'].$this->dependences()[$class].'.php');
            }
        );
    }

    private function dependences() {
        return [
            'Client' => 'collections/Client',
            'ClientControllers' => 'modules/client/controllers/ClientControllers',
            'ClientMappers' => 'modules/client/mappers/ClientMappers',
            'Connection' => 'library/Connection',
            'Item' => 'collections/Item',
            'ItemControllers' => 'modules/item/controllers/ItemControllers',
            'ItemMappers' => 'modules/item/mappers/ItemMappers',
            'Method' => 'library/Method',
            'Product' => 'collections/Product',
            'ProductControllers' => 'modules/product/controllers/ProductControllers',
            'ProductMappers' => 'modules/product/mappers/ProductMappers',
            'Route' => 'library/Route',
            'Sell' => 'collections/Sell',
            'SellControllers' => 'modules/sell/controllers/SellControllers',
            'SellMappers' => 'modules/sell/mappers/SellMappers',
            'Tax' => 'collections/Tax',
            'TaxControllers' => 'modules/tax/controllers/TaxControllers',
            'TaxMappers' => 'modules/tax/mappers/TaxMappers',
            'Token' => 'library/Token',
            'Type' => 'collections/Type',
            'TypeControllers' => 'modules/type/controllers/TypeControllers',
            'TypeMappers' => 'modules/type/mappers/TypeMappers',
        ];
    }
}
