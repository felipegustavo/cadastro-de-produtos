<?php

interface Router
{
    public function routes($route, $params);
    public function methods();
}
