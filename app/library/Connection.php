<?php

class Connection
{
    public static $conn;

    public function getInstance()
    {
        if (!isset(self::$instance)) {
            $params = sprintf(
                'pgsql:host=%s; port=%d; dbname=%s; user=%s; password=%s',
                'localhost',
                5432,
                'se',
                'postgres',
                '32130'
            );

            self::$conn = new \PDO($params);
        }

        return self::$conn;
    }
}
