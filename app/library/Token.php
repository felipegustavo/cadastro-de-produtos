<?php

Class Token
{
    public function validateToken($token)
    {
        $sql = "SELECT key FROM token WHERE key =:key";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute(['key' => $token]);

        return (bool) $stmt->fetch(PDO::FETCH_ASSOC)['key'];
    }
}
