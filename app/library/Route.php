<?php

class Route
{
    public function routes($uri, $params = null)
    {
        $route = $this->abstractRoute($uri, $params);

        if (isset($route['error'])) {
            return $route['error'];
        }

        $class = new $route['collection'];

        if (!class_exists($route['collection'])) {
            return 'Class not exist';
        }

        unset($params['token']);

        return $class->routes($route['route'], $params);
    }

    private function abstractRoute($uri)
    {
        if (!isset(explode('/api/collections/', $uri)[1])) {
            return [
                'error' => 'Route invalide',
            ];
        }

        $uri = explode('/api/collections/', $uri)[1];
        $collection = explode('/', $uri)[0];
        $route = '/'.explode('?', explode('/', $uri)[1])[0];

        return [
            'collection' => ucfirst($collection),
            'route' => $route,
        ];
    }
}
