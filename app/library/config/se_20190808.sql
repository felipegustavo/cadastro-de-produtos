--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE client (
    id_client integer NOT NULL,
    name character(100)
);


ALTER TABLE client OWNER TO postgres;

--
-- Name: client_id_client_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE client_id_client_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_client_seq OWNER TO postgres;

--
-- Name: client_id_client_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE client_id_client_seq OWNED BY client.id_client;


--
-- Name: itens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE itens (
    id_item integer,
    id_sell integer,
    id_product integer,
    amount integer,
    situation integer DEFAULT 0
);


ALTER TABLE itens OWNER TO postgres;

--
-- Name: itens_id_item_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE itens_id_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itens_id_item_seq OWNER TO postgres;

--
-- Name: itens_id_item_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE itens_id_item_seq OWNED BY itens.id_item;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products (
    id_product integer,
    name character(100),
    id_type integer,
    value numeric,
    situation integer DEFAULT 0
);


ALTER TABLE products OWNER TO postgres;

--
-- Name: products_has_tax; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_has_tax (
    id_product integer,
    id_tax integer,
    situation integer DEFAULT 0,
    id_products_has_tax integer
);


ALTER TABLE products_has_tax OWNER TO postgres;

--
-- Name: products_has_tax_id_products_has_tax_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_has_tax_id_products_has_tax_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_has_tax_id_products_has_tax_seq OWNER TO postgres;

--
-- Name: products_has_tax_id_products_has_tax_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_has_tax_id_products_has_tax_seq OWNED BY products_has_tax.id_products_has_tax;


--
-- Name: products_id_product_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_id_product_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_product_seq OWNER TO postgres;

--
-- Name: products_id_product_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_id_product_seq OWNED BY products.id_product;


--
-- Name: sell; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sell (
    id_sell integer,
    id_client integer,
    situation integer DEFAULT 0,
    sell_date date
);


ALTER TABLE sell OWNER TO postgres;

--
-- Name: sell_id_sell_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sell_id_sell_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sell_id_sell_seq OWNER TO postgres;

--
-- Name: sell_id_sell_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sell_id_sell_seq OWNED BY sell.id_sell;


--
-- Name: tax; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tax (
    id_tax integer,
    name character(100),
    percentage integer,
    situation integer DEFAULT 0
);


ALTER TABLE tax OWNER TO postgres;

--
-- Name: tax_id_tax_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tax_id_tax_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tax_id_tax_seq OWNER TO postgres;

--
-- Name: tax_id_tax_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tax_id_tax_seq OWNED BY tax.id_tax;


--
-- Name: token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE token (
    key character(250)
);


ALTER TABLE token OWNER TO postgres;

--
-- Name: type_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE type_products (
    id_type integer,
    name character(100),
    situation integer DEFAULT 0
);


ALTER TABLE type_products OWNER TO postgres;

--
-- Name: type_products_id_type_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE type_products_id_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE type_products_id_type_seq OWNER TO postgres;

--
-- Name: type_products_id_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE type_products_id_type_seq OWNED BY type_products.id_type;


--
-- Name: id_client; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client ALTER COLUMN id_client SET DEFAULT nextval('client_id_client_seq'::regclass);


--
-- Name: id_item; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY itens ALTER COLUMN id_item SET DEFAULT nextval('itens_id_item_seq'::regclass);


--
-- Name: id_product; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products ALTER COLUMN id_product SET DEFAULT nextval('products_id_product_seq'::regclass);


--
-- Name: id_products_has_tax; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_has_tax ALTER COLUMN id_products_has_tax SET DEFAULT nextval('products_has_tax_id_products_has_tax_seq'::regclass);


--
-- Name: id_sell; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sell ALTER COLUMN id_sell SET DEFAULT nextval('sell_id_sell_seq'::regclass);


--
-- Name: id_tax; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tax ALTER COLUMN id_tax SET DEFAULT nextval('tax_id_tax_seq'::regclass);


--
-- Name: id_type; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY type_products ALTER COLUMN id_type SET DEFAULT nextval('type_products_id_type_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO client VALUES (1, 'Felipe                                                                                              ');
INSERT INTO client VALUES (2, 'Pedro                                                                                               ');
INSERT INTO client VALUES (3, 'Bianca                                                                                              ');


--
-- Name: client_id_client_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('client_id_client_seq', 3, true);


--
-- Data for Name: itens; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO itens VALUES (1, 1, 1, 2, 0);
INSERT INTO itens VALUES (2, 1, 5, 5, 0);
INSERT INTO itens VALUES (3, 1, 10, 7, 0);
INSERT INTO itens VALUES (4, 1, 15, 2, 0);
INSERT INTO itens VALUES (5, 2, 7, 15, 0);
INSERT INTO itens VALUES (6, 2, 11, 10, 0);
INSERT INTO itens VALUES (7, 2, 12, 5, 0);
INSERT INTO itens VALUES (8, 2, 14, 2, 0);
INSERT INTO itens VALUES (9, 3, 2, 2, 0);
INSERT INTO itens VALUES (10, 3, 12, 5, 0);
INSERT INTO itens VALUES (11, 3, 6, 2, 0);
INSERT INTO itens VALUES (12, 4, 1, 2, 0);
INSERT INTO itens VALUES (13, 4, 9, 3, 0);
INSERT INTO itens VALUES (14, 4, 3, 1, 0);
INSERT INTO itens VALUES (15, 4, 4, 5, 0);


--
-- Name: itens_id_item_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('itens_id_item_seq', 15, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO products VALUES (1, 'Coca Cola                                                                                           ', 1, 4.50, 0);
INSERT INTO products VALUES (2, 'Cerveja                                                                                             ', 1, 3.75, 0);
INSERT INTO products VALUES (3, 'Suco                                                                                                ', 1, 5.30, 0);
INSERT INTO products VALUES (4, 'Bovina                                                                                              ', 2, 17.80, 0);
INSERT INTO products VALUES (5, 'Frango                                                                                              ', 2, 15.80, 0);
INSERT INTO products VALUES (6, 'Porco                                                                                               ', 2, 7.40, 0);
INSERT INTO products VALUES (7, 'Fio Dental                                                                                          ', 3, 2.54, 0);
INSERT INTO products VALUES (8, 'Fralda                                                                                              ', 3, 19.90, 0);
INSERT INTO products VALUES (9, 'Abacate                                                                                             ', 4, 5.50, 0);
INSERT INTO products VALUES (10, 'Goiaba                                                                                              ', 4, 2.75, 0);
INSERT INTO products VALUES (11, 'Cafe                                                                                                ', 5, 9.80, 0);
INSERT INTO products VALUES (12, 'Leite                                                                                               ', 5, 3.85, 0);
INSERT INTO products VALUES (13, 'Fermento                                                                                            ', 5, 4.40, 0);
INSERT INTO products VALUES (14, 'Arroz                                                                                               ', 5, 6.30, 0);
INSERT INTO products VALUES (15, 'Cereal                                                                                              ', 5, 21.52, 0);


--
-- Data for Name: products_has_tax; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO products_has_tax VALUES (1, 1, 0, 1);
INSERT INTO products_has_tax VALUES (1, 2, 0, 2);
INSERT INTO products_has_tax VALUES (2, 3, 0, 3);
INSERT INTO products_has_tax VALUES (3, 2, 0, 4);
INSERT INTO products_has_tax VALUES (4, 1, 0, 5);
INSERT INTO products_has_tax VALUES (5, 2, 0, 6);
INSERT INTO products_has_tax VALUES (6, 3, 0, 7);
INSERT INTO products_has_tax VALUES (7, 1, 0, 8);
INSERT INTO products_has_tax VALUES (7, 2, 0, 9);
INSERT INTO products_has_tax VALUES (8, 3, 0, 9);
INSERT INTO products_has_tax VALUES (9, 2, 0, 10);
INSERT INTO products_has_tax VALUES (9, 3, 0, 11);
INSERT INTO products_has_tax VALUES (10, 1, 0, 12);
INSERT INTO products_has_tax VALUES (11, 2, 0, 13);
INSERT INTO products_has_tax VALUES (12, 1, 0, 14);
INSERT INTO products_has_tax VALUES (13, 2, 0, 15);
INSERT INTO products_has_tax VALUES (14, 1, 0, 16);
INSERT INTO products_has_tax VALUES (15, 1, 0, 17);
INSERT INTO products_has_tax VALUES (15, 2, 0, 18);
INSERT INTO products_has_tax VALUES (15, 3, 0, 19);


--
-- Name: products_has_tax_id_products_has_tax_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_has_tax_id_products_has_tax_seq', 15, true);


--
-- Name: products_id_product_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_id_product_seq', 15, true);


--
-- Data for Name: sell; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sell VALUES (1, 1, 0, '2019-08-08');
INSERT INTO sell VALUES (2, 2, 0, '2019-08-08');
INSERT INTO sell VALUES (3, 1, 0, '2019-08-08');
INSERT INTO sell VALUES (4, 3, 0, '2019-08-08');


--
-- Name: sell_id_sell_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sell_id_sell_seq', 4, true);


--
-- Data for Name: tax; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tax VALUES (1, 'PIS                                                                                                 ', 5, 0);
INSERT INTO tax VALUES (2, 'COFINS                                                                                              ', 10, 0);
INSERT INTO tax VALUES (3, 'ICMS                                                                                                ', 15, 0);


--
-- Name: tax_id_tax_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tax_id_tax_seq', 3, true);


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO token VALUES ('qUFVBL1A478LJT0OD6HF209R1D0MXXA07V0RZYM4EJIZRBV3Q155WGP40FA3MQWXIQBMVMBECD43ON8LG3EQHFFF02X02ZBJQVK3                                                                                                                                                      ');


--
-- Data for Name: type_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO type_products VALUES (1, 'Bebida                                                                                              ', 0);
INSERT INTO type_products VALUES (2, 'Carnes                                                                                              ', 0);
INSERT INTO type_products VALUES (3, 'Higiene                                                                                             ', 0);
INSERT INTO type_products VALUES (4, 'Fruta                                                                                               ', 0);
INSERT INTO type_products VALUES (5, 'Alimento                                                                                            ', 0);


--
-- Name: type_products_id_type_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('type_products_id_type_seq', 5, true);


--
-- Name: index_id_client; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_client ON public.client USING btree (id_client);


--
-- Name: index_id_client_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_client_2 ON public.sell USING btree (id_client);


--
-- Name: index_id_item; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_item ON public.itens USING btree (id_item);


--
-- Name: index_id_product; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_product ON public.products USING btree (id_product);


--
-- Name: index_id_product_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_product_2 ON public.itens USING btree (id_product);


--
-- Name: index_id_product_3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_product_3 ON public.products_has_tax USING btree (id_product);


--
-- Name: index_id_products_has_tax; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_products_has_tax ON public.products_has_tax USING btree (id_products_has_tax);


--
-- Name: index_id_sell; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_sell ON public.sell USING btree (id_sell);


--
-- Name: index_id_sell_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_sell_2 ON public.itens USING btree (id_sell);


--
-- Name: index_id_tax; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_tax ON public.tax USING btree (id_tax);


--
-- Name: index_id_tax_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_tax_2 ON public.products_has_tax USING btree (id_tax);


--
-- Name: index_id_type; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_id_type ON public.type_products USING btree (id_type);


--
-- Name: index_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_key ON public.token USING btree (key);


--
-- Name: index_type_products_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_type_products_id ON public.products USING btree (id_type);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

