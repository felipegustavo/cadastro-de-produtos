<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Type implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $typeControllers = new TypeControllers;
        if (!method_exists($typeControllers, $method)) {
            return "Method not exist";
        }

        return $typeControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/update' => 'update',
            '/get' => 'get',
            '/delete' => 'delete',
        ];
    }
}
