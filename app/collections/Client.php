<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Client implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $clientControllers = new ClientControllers;
        if (!method_exists($clientControllers, $method)) {
            return "Method not exist";
        }

        return $clientControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/get' => 'get',
        ];
    }
}
