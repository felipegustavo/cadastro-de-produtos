<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Sell implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $sellControllers = new SellControllers;
        if (!method_exists($sellControllers, $method)) {
            return "Method not exist";
        }

        return $sellControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/get' => 'get',
            '/delete' => 'delete',
            '/list' => 'listSell',
            '/listtotal' => 'listSellTotal',
        ];
    }
}
