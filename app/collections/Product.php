<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Product implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $productControllers = new ProductControllers;
        if (!method_exists($productControllers, $method)) {
            return "Method not exist";
        }

        return $productControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/update' => 'update',
            '/get' => 'get',
            '/delete' => 'delete',
            '/producthaxtax' => 'productHaxTax',
            '/producthaxtaxdelete' => 'productHaxTaxDelete',
        ];
    }
}
