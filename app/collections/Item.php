<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Item implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $itemControllers = new ItemControllers;
        if (!method_exists($itemControllers, $method)) {
            return "Method not exist";
        }

        return $itemControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/update' => 'update',
            '/get' => 'get',
            '/delete' => 'delete',
        ];
    }
}
