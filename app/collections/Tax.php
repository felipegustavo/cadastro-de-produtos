<?php

require_once($_SERVER['DOCUMENT_ROOT'].'library/interface/Router.php');

class Tax implements Router
{
    public function routes($route, $params)
    {
        $method = $this->methods()[$route];

        $taxControllers = new TaxControllers;
        if (!method_exists($taxControllers, $method)) {
            return "Method not exist";
        }

        return $taxControllers->$method($params);
    }

    public function methods() {
        return [
            '/' => 'create',
            '/update' => 'update',
            '/get' => 'get',
            '/delete' => 'delete',
        ];
    }
}
