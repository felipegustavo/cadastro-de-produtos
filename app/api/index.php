<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'library/library.php');

    $method = new Method();
    $params = $method->define($_SERVER['REQUEST_METHOD']);
    if (isset($params['error'])) {
        echo $params['error'];
    }

    $token = new Token();
    $validate = $token->validateToken($params['token']);

    if (false === $validate) {
        echo 'Token invalid';
    }

    if (true === $validate) {
        $route = new Route();
        $api = $route->routes($_SERVER['REQUEST_URI'], $params);

        echo json_encode($api, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
