<?php

Class ClientMappers
{
    public function create($params)
    {
        $params['name'] = (string) $params['name'];

        $sql = "INSERT INTO client (name) VALUES (:name)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'name' => $params['name'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function update($params)
    {
        $params['id_client'] = (int) $params['id_client'];
        $params['name'] = (string) $params['name'];

        $sql = "UPDATE client SET name = :name WHERE id_client = :id_client";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_client' => $params['id_client'],
            'name' => $params['name'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_client', $params)) {
            $filter = implode(',', $params['id_client']);
        }

        $sql = "SELECT * FROM client";
        if (isset($filter)) {
            $sql .= " WHERE id_client IN($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_client = implode(',', $params['id_client']);

        $sql = "UPDATE client SET situation = 1 WHERE id_client IN ($ids_client)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }
}
