<?php

Class ClientControllers
{
    public function create($params)
    {
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }

        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }

        $client = new ClientMappers();
        return $client->create($params);
    }

    public function get($params)
    {
        $client = new ClientMappers();
        return $client->get($params);
    }
}
