<?php

Class SellMappers
{
    public function create($params)
    {
        $params['id_client'] = (int) $params['id_client'];

        $paramFilter['id_client'] = [$params['id_client']];
        $clientMapper = new ClientMappers();
        $client = $clientMapper->get($paramFilter);

        if (empty($client)) {
            return [
                'error' => 'not exist',
                'message' => 'CLIENT not exist',
            ];
        }

        $sql = "INSERT INTO sell (id_client, sell_date) VALUES (:id_client, :sell_date)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_client' => $params['id_client'],
            'sell_date' => date('Y-m-d'),
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_sell', $params)
            && is_array($params['id_sell'])) {
            $filter = implode(',', $params['id_sell']);
        }
        if (isset($params['id_sell']) &&
            !is_array($params['id_sell'])) {
            $filter = $params['id_sell'];
        }

        $sql = "SELECT
                    sell.id_sell,
                    sell.id_client,
                    sell.sell_date,
                    sell.situation,
                    client.name AS name_client
                FROM sell AS sell
                INNER JOIN client AS client ON client.id_client = sell.id_client";
        if (isset($filter)) {
            $sql .= " WHERE id_sell IN($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_sell = implode(',', $params['id_sell']);

        $sql = "UPDATE sell SET situation = 1 WHERE id_sell IN ($ids_sell)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }

    public function listSell($params)
    {
        if (array_key_exists('id_sell', $params)
            && is_array($params['id_sell'])) {
            $filter = implode(',', $params['id_sell']);
        }
        if (isset($params['id_sell']) &&
            !is_array($params['id_sell'])) {
            $filter = $params['id_sell'];
        }

        $sql = "SELECT
                    itens.id_item,
                    client.name AS name_client,
                    products.name AS name_product,
                    type_products.name AS type_product,
                    SUM(itens.amount) AS amount,
                    SUM(CAST(products.value AS money)) AS value_item,
                    SUM(CAST((products.value * itens.amount) AS money)) AS value_total,
                    SUM(CAST((((tax.percentage * products.value) / 100) * itens.amount) AS money)) AS value_tax
                FROM client AS client
                INNER JOIN sell AS sell ON sell.id_client = client.id_client AND sell.situation = 0
                INNER JOIN itens AS itens ON itens.id_sell =  sell.id_sell AND itens.situation = 0
                INNER JOIN products AS products ON products.id_product = itens.id_product
                INNER JOIN type_products AS type_products ON type_products.id_type = products.id_type
                INNER JOIN products_has_tax AS products_has_tax ON products_has_tax.id_product = products.id_product
                LEFT JOIN tax AS tax ON tax.id_tax = products_has_tax.id_tax";

        if (isset($filter)) {
            $sql .= " WHERE sell.id_sell IN ($filter)";
        }

        $sql .= " GROUP BY itens.id_item, sell.id_sell, products.name, client.name, type_products.name
                  ORDER BY value_total DESC, value_tax DESC";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function listSellTotal($params)
    {
        if (array_key_exists('id_sell', $params)
            && is_array($params['id_sell'])) {
            $filter = implode(',', $params['id_sell']);
        }
        if (isset($params['id_sell']) &&
            !is_array($params['id_sell'])) {
            $filter = $params['id_sell'];
        }

        $sql = "SELECT
                    *
                FROM(
                (
                        SELECT
                            sell.id_sell,
                            client.name AS name_client,
                            SUM(cast((products.value * itens.amount) AS money)) AS value_total,
                            SUM(cast((((tax.percentage * products.value) / 100) * itens.amount) AS money)) AS value_tax,
                            to_char(sell.sell_date, 'DD/MM/YYYY') AS sell_date
                        FROM client AS client
                        INNER JOIN sell AS sell ON sell.id_client = client.id_client AND sell.situation = 0
                        INNER JOIN itens AS itens ON itens.id_sell =  sell.id_sell AND itens.situation = 0
                        INNER JOIN products AS products ON products.id_product = itens.id_product
                        INNER JOIN type_products AS type_products ON type_products.id_type = products.id_type
                        INNER JOIN products_has_tax AS products_has_tax ON products_has_tax.id_product = products.id_product
                        LEFT JOIN tax AS tax ON tax.id_tax = products_has_tax.id_tax
                        GROUP BY sell.id_sell, client.name, sell.sell_date
                        ORDER BY client.name ASC, value_total DESC, value_tax DESC
                    )
                    UNION
                    (
                        SELECT
                            sell.id_sell,
                            client.name AS name_client,
                            CAST((0) AS money) AS value_total,
                            CAST((0) AS money) AS value_tax,
                            to_char(sell.sell_date, 'DD/MM/YYYY') AS sell_date
                        FROM sell AS sell
                        LEFT JOIN client AS client ON client.id_client = sell.id_client
                        LEFT JOIN itens AS itens ON itens.id_sell = sell.id_sell
                        WHERE
                            sell.situation = 0 AND
                            itens.id_item IS NULL
                    )
                ) AS dataquery";

        if (isset($filter)) {
            $sql .= " WHERE dataquery.id_sell IN ($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
