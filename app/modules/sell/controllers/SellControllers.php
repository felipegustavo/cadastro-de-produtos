<?php

Class SellControllers
{
    public function create($params)
    {
        if (!isset($params['id_client'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_CLIENT is not exists',
            ];
        }

        if (empty($params['id_client'])) {
            return [
                'error' => 'null',
                'message' => 'ID_CLIENT is null',
            ];
        }

        $sell = new SellMappers();
        return $sell->create($params);
    }

    public function get($params)
    {
        $sell = new SellMappers();
        return $sell->get($params);
    }

    public function delete($params)
    {
        if (!isset($params['id_sell'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_SELL is not exists',
            ];
        }

        if (empty($params['id_sell'])) {
            return [
                'error' => 'null',
                'message' => 'ID_SELL is null',
            ];
        }

        $sell = new SellMappers();
        return $sell->delete($params);
    }

    public function listSell($params)
    {
        $sell = new SellMappers();
        return $sell->listSell($params);
    }

    public function listSellTotal($params)
    {
        $sell = new SellMappers();
        return $sell->listSellTotal($params);
    }
}
