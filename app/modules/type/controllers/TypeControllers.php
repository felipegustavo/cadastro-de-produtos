<?php

Class TypeControllers
{
    public function create($params)
    {
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }

        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }

        $type = new TypeMappers();
        return $type->create($params);
    }

    public function update($params)
    {
        if (!isset($params['id_type'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TYPE is not exists',
            ];
        }
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }

        if (empty($params['id_type'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TYPE is null',
            ];
        }
        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }

        $type = new TypeMappers();
        return $type->update($params);
    }

    public function get($params)
    {
        $type = new TypeMappers();
        return $type->get($params);
    }

    public function delete($params)
    {
        if (!isset($params['id_type'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TYPE is not exists',
            ];
        }

        if (empty($params['id_type'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TYPE is null',
            ];
        }

        $type = new TypeMappers();
        return $type->delete($params);
    }
}
