<?php

Class TypeMappers
{
    public function create($params)
    {
        $params['name'] = (string) $params['name'];

        $sql = "INSERT INTO type_products (name) VALUES (:name)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'name' => $params['name'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function update($params)
    {
        $params['id_type'] = (int) $params['id_type'];
        $params['name'] = (string) $params['name'];

        $sql = "UPDATE type_products SET name = :name WHERE id_type = :id_type";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_type' => $params['id_type'],
            'name' => $params['name'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_type', $params)) {
            $filter = implode(',', $params['id_type']);
        }

        $sql = "SELECT * FROM type_products ";
        $sql .= " WHERE 1=1 ";

        if (isset($params['situation'])) {
            $sql .= " AND situation = ".$params['situation'];
        }
        if (isset($filter)) {
            $sql .= " AND id_type IN($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_type = implode(',', $params['id_type']);

        $sql = "UPDATE type_products SET situation = 1 WHERE id_type IN ($ids_type)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }
}
