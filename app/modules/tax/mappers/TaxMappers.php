<?php

Class TaxMappers
{
    public function create($params)
    {
        $params['name'] = (string) $params['name'];
        $params['percentage'] = (string) $params['percentage'];

        $sql = "INSERT INTO tax (name, percentage) VALUES (:name, :percentage)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'name' => $params['name'],
            'percentage' => $params['percentage'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function update($params)
    {
        $params['id_tax'] = (int) $params['id_tax'];
        $params['name'] = (string) $params['name'];
        $params['percentage'] = (int) $params['percentage'];

        $sql = "UPDATE tax SET name = :name, percentage = :percentage WHERE id_tax = :id_tax";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_tax' => $params['id_tax'],
            'name' => $params['name'],
            'percentage' => $params['percentage'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_tax', $params)) {
            $filter = implode(',', $params['id_tax']);
        }

        $sql = "SELECT * FROM tax AS tax ";

        if (isset($params['id_product'])) {
            $sql .= " INNER JOIN products_has_tax AS products_has_tax ON
                        products_has_tax.situation = 0 AND
                        products_has_tax.id_tax = tax.id_tax AND
                        products_has_tax.id_product = ".$params['id_product'];
        }

        $sql .= " WHERE 1=1 ";
        if (isset($params['situation'])) {
            $sql .= " AND tax.situation = ".$params['situation'];
        }

        if (isset($filter)) {
            $sql .= " AND id_tax IN($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_tax = implode(',', $params['id_tax']);

        $sql = "UPDATE tax SET situation = 1 WHERE id_tax IN ($ids_tax)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }
}
