<?php

Class TaxControllers
{
    public function create($params)
    {
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }
        if (!isset($params['percentage'])) {
            return [
                'error' => 'undefined',
                'message' => 'PERCENTAGE is not exists',
            ];
        }

        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }
        if (empty($params['percentage'])) {
            return [
                'error' => 'null',
                'message' => 'PERCENTAGE is null',
            ];
        }

        $tax = new TaxMappers();
        return $tax->create($params);
    }

    public function update($params)
    {
        if (!isset($params['id_tax'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TAX is not exists',
            ];
        }
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }
        if (!isset($params['percentage'])) {
            return [
                'error' => 'undefined',
                'message' => 'PERCENTAGE is not exists',
            ];
        }

        if (empty($params['id_tax'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TAX is null',
            ];
        }
        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }
        if (empty($params['percentage'])) {
            return [
                'error' => 'null',
                'message' => 'PERCENTAGE is null',
            ];
        }

        $tax = new TaxMappers();
        return $tax->update($params);
    }

    public function get($params)
    {
        $tax = new TaxMappers();
        return $tax->get($params);
    }

    public function delete($params)
    {
        if (!isset($params['id_tax'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TAX is not exists',
            ];
        }

        if (empty($params['id_tax'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TAX is null',
            ];
        }

        $tax = new TaxMappers();
        return $tax->delete($params);
    }
}
