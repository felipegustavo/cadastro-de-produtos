<?php

Class ItemMappers
{
    public function create($params)
    {
        $params['id_sell'] = (int) $params['id_sell'];
        $params['id_product'] = (int) $params['id_product'];
        $params['amount'] = (int) $params['amount'];

        $paramFilter['id_sell'] = [$params['id_sell']];
        $sellMapper = new SellMappers();
        $sell = $sellMapper->get($paramFilter);

        if (empty($sell)) {
            return [
                'error' => 'not exist',
                'message' => 'SELL not exist',
            ];
        }

        if (1 === reset($sell)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'SELL excluded',
            ];
        }

        $paramFilter['id_product'] = [$params['id_product']];
        $productMapper = new ProductMappers();
        $product = $productMapper->get($paramFilter);

        if (empty($product)) {
            return [
                'error' => 'not exist',
                'message' => 'PRODUCT not exist',
            ];
        }

        if (1 === reset($product)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'PRODUCT excluded',
            ];
        }

        $sql = "INSERT INTO itens (id_product, id_sell, amount) VALUES (:id_product, :id_sell, :amount)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_product' => $params['id_product'],
            'id_sell' => $params['id_sell'],
            'amount' => $params['amount'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function update($params)
    {
        $params['id_item'] = (int) $params['id_item'];
        $params['amount'] = (int) $params['amount'];

        $sql = "UPDATE itens SET amount = :amount WHERE id_item = :id_item";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_item' => $params['id_item'],
            'amount' => $params['amount'],
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_item', $params)) {
            $filter = implode(',', $params['id_item']);
        }

        $sql = "SELECT * FROM itens";
        if (isset($filter)) {
            $sql .= " WHERE id_item IN($filter)";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_item = implode(',', $params['id_item']);

        $sql = "UPDATE itens SET situation = 1 WHERE id_item IN ($ids_item)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }
}
