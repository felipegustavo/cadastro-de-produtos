<?php

Class ItemControllers
{
    public function create($params)
    {
        if (!isset($params['id_sell'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_SELL is not exists',
            ];
        }
        if (!isset($params['id_product'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_PRODUCT is not exists',
            ];
        }
        if (!isset($params['amount'])) {
            return [
                'error' => 'undefined',
                'message' => 'AMOUNT is not exists',
            ];
        }

        if (empty($params['id_sell'])) {
            return [
                'error' => 'null',
                'message' => 'ID_SELL is null',
            ];
        }
        if (empty($params['id_product'])) {
            return [
                'error' => 'null',
                'message' => 'ID_PRODUCT is null',
            ];
        }
        if (empty($params['amount'])) {
            return [
                'error' => 'null',
                'message' => 'AMOUNT is null',
            ];
        }

        $item = new ItemMappers();
        return $item->create($params);
    }

    public function update($params)
    {
        if (!isset($params['id_item'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_ITEM is not exists',
            ];
        }
        if (!isset($params['amount'])) {
            return [
                'error' => 'undefined',
                'message' => 'AMOUNT is not exists',
            ];
        }

        if (empty($params['id_item'])) {
            return [
                'error' => 'null',
                'message' => 'ID_ITEM is null',
            ];
        }
        if (empty($params['amount'])) {
            return [
                'error' => 'null',
                'message' => 'AMOUNT is null',
            ];
        }

        $item = new ItemMappers();
        return $item->update($params);
    }

    public function get($params)
    {
        $item = new ItemMappers();
        return $item->get($params);
    }

    public function delete($params)
    {
        if (!isset($params['id_item'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_ITEM is not exists',
            ];
        }

        if (empty($params['id_item'])) {
            return [
                'error' => 'null',
                'message' => 'ID_ITEM is null',
            ];
        }

        $item = new ItemMappers();
        return $item->delete($params);
    }
}
