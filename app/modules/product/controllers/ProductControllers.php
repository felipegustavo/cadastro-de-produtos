<?php

Class ProductControllers
{
    public function create($params)
    {
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }
        if (!isset($params['id_type'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TYPE is not exists',
            ];
        }
        if (!isset($params['value'])) {
            return [
                'error' => 'undefined',
                'message' => 'VALUE is not exists',
            ];
        }

        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }
        if (empty($params['id_type'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TYPE is null',
            ];
        }
        if (empty($params['value'])) {
            return [
                'error' => 'null',
                'message' => 'VALUE is null',
            ];
        }

        $product = new ProductMappers();
        return $product->create($params);
    }

    public function update($params)
    {
        if (!isset($params['id_product'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_PRODUCT is not exists',
            ];
        }
        if (!isset($params['name'])) {
            return [
                'error' => 'undefined',
                'message' => 'NAME is not exists',
            ];
        }
        if (!isset($params['id_type'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TYPE is not exists',
            ];
        }
        if (!isset($params['value'])) {
            return [
                'error' => 'undefined',
                'message' => 'VALUE is not exists',
            ];
        }

        if (empty($params['id_product'])) {
            return [
                'error' => 'null',
                'message' => 'ID_PRODUCT is null',
            ];
        }
        if (empty($params['name'])) {
            return [
                'error' => 'null',
                'message' => 'NAME is null',
            ];
        }
        if (empty($params['id_type'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TYPE is null',
            ];
        }
        if (empty($params['value'])) {
            return [
                'error' => 'null',
                'message' => 'VALUE is null',
            ];
        }

        $product = new ProductMappers();
        return $product->update($params);
    }

    public function get($params)
    {
        $product = new ProductMappers();
        return $product->get($params);
    }

    public function delete($params)
    {
        if (!isset($params['id_product'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_PRODUCT is not exists',
            ];
        }

        if (empty($params['id_product'])) {
            return [
                'error' => 'null',
                'message' => 'ID_PRODUCT is null',
            ];
        }

        $product = new ProductMappers();
        return $product->delete($params);
    }

    public function productHaxTax($params)
    {
        if (!isset($params['id_product'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_PRODUCT is not exists',
            ];
        }
        if (!isset($params['id_tax'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TAX is not exists',
            ];
        }

        if (empty($params['id_product'])) {
            return [
                'error' => 'null',
                'message' => 'ID_PRODUCT is null',
            ];
        }
        if (empty($params['id_tax'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TAX is not exists',
            ];
        }

        $productHaxTax = new ProductMappers();
        return $productHaxTax->productHaxTax($params);
    }

    public function productHaxTaxDelete($params)
    {
        if (!isset($params['id_tax'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_TAX is not exists',
            ];
        }
        if (!isset($params['id_product'])) {
            return [
                'error' => 'undefined',
                'message' => 'ID_PRODUCT is not exists',
            ];
        }

        if (empty($params['id_tax'])) {
            return [
                'error' => 'null',
                'message' => 'ID_TAX is null',
            ];
        }
        if (empty($params['id_product'])) {
            return [
                'error' => 'null',
                'message' => 'ID_PRODUCT is null',
            ];
        }

        $productHaxTaxDelete = new ProductMappers();
        return $productHaxTaxDelete->productHaxTaxDelete($params);
    }
}
