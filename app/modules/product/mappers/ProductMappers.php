<?php

Class ProductMappers
{
    public function create($params)
    {
        $params['name'] = (string) $params['name'];
        $params['id_type'] = (int) $params['id_type'];
        $params['value'] = (float) $params['value'];

        $paramFilter['id_type'] = [$params['id_type']];
        $type = new TypeMappers();
        $typeProduct = $type->get($paramFilter);

        if (empty($typeProduct)) {
            return [
                'error' => 'not exist',
                'message' => 'TYPE not exist',
            ];
        }

        if (1 === reset($typeProduct)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'TYPE excluded',
            ];
        }

        $sql = "INSERT INTO products (name, id_type, value) VALUES (:name, :id_type, :value)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'name' => $params['name'],
            'id_type' => $params['id_type'],
            'value' => $params['value']
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function update($params)
    {
        $params['id_product'] = (int) $params['id_product'];
        $params['name'] = (string) $params['name'];
        $params['id_type'] = (int) $params['id_type'];
        $params['value'] = (float) $params['value'];

        $paramFilter['id_type'] = [$params['id_type']];
        $type = new TypeMappers();
        $typeProduct = $type->get($paramFilter);

        if (empty($typeProduct)) {
            return [
                'error' => 'not exist',
                'message' => 'TYPE not exist',
            ];
        }

        if (1 === reset($typeProduct)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'TYPE excluded',
            ];
        }

        $sql = "UPDATE products SET name = :name, id_type = :id_type, value = :value WHERE id_product = :id_product";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_product' => $params['id_product'],
            'name' => $params['name'],
            'id_type' => $params['id_type'],
            'value' => $params['value']
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function get($params)
    {
        if (array_key_exists('id_product', $params)
            && is_array($params['id_product'])) {
            $filter = implode(',', $params['id_product']);
        }
        if (isset($params['id_product']) &&
            !is_array($params['id_product'])) {
            $filter = $params['id_product'];
        }

        $sql = "SELECT
                    products.id_product,
                    products.id_type,
                    type_products.name AS name_type_product,
                    products.name,
                    products.situation,
                    CAST(products.value AS money) AS value
                FROM products AS products ";
        $sql .= " INNER JOIN type_products AS type_products ON type_products.id_type = products.id_type ";
        $sql .= " WHERE 1=1 ";

        if (isset($params['situation'])) {
            $sql .= " AND products.situation = ".$params['situation'];
        }
        if (isset($filter)) {
            $sql .= " AND products.id_product IN ($filter) ";
        }

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($params)
    {
        $ids_product = implode(',', $params['id_product']);

        $sql = "UPDATE products SET situation = 1 WHERE id_product IN ($ids_product)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }

    public function productHaxTax($params)
    {
        $params['id_product'] = (int) $params['id_product'];
        $params['id_tax'] = (int) $params['id_tax'];

        $paramFilter['id_product'] = [$params['id_product']];
        $productMapper = new ProductMappers();
        $product = $productMapper->get($paramFilter);

        if (empty($product)) {
            return [
                'error' => 'not exist',
                'message' => 'PRODUCT not exist',
            ];
        }

        if (1 === reset($product)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'PRODUCT excluded',
            ];
        }

        $paramFilter['id_tax'] = [$params['id_tax']];
        $taxMapper = new TaxMappers();
        $tax = $taxMapper->get([$paramFilter['id_tax']]);

        if (empty($tax)) {
            return [
                'error' => 'not exist',
                'message' => 'TAX not exist',
            ];
        }

        if (1 === reset($tax)['situation']) {
            return [
                'error' => 'excluded',
                'message' => 'TAX excluded',
            ];
        }

        $sql = "INSERT INTO products_has_tax (id_product, id_tax) VALUES (:id_product, :id_tax)";

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);

        $stmt->execute([
            'id_product' => $params['id_product'],
            'id_tax' => $params['id_tax']
        ]);

        return [
            'status' => 'success',
            'row' => $params,
        ];
    }

    public function productHaxTaxDelete($params)
    {
        $ids_tax = implode(',', $params['id_tax']);

        $sql = "UPDATE
                    products_has_tax SET situation = 1
                WHERE
                    id_tax IN ($ids_tax) AND id_product = ".$params['id_product'];

        $conn = new Connection();
        $stmt = $conn
            ->getInstance()
            ->prepare($sql);
        $stmt->execute();

        return [
            'status' => 'deleted',
        ];
    }
}
